package com.luismarin.celtacys;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity{
//public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int PICK_PDF_CODE = 1000;
    Button btn_open_assets,btn_open_storage,btn_open_web,btn_download;
    int count =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Request Read & Write
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new BaseMultiplePermissionsListener(){
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        super.onPermissionsChecked(report);
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        super.onPermissionRationaleShouldBeShown(permissions, token);
                    }
                }).check();

        btn_open_assets = (Button)findViewById(R.id.btn_open_assets);
        btn_open_storage = (Button)findViewById(R.id.btn_open_storage);
        btn_open_web = (Button)findViewById(R.id.btn_open_web);
        btn_download = (Button)findViewById(R.id.btn_download);

        btn_open_assets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                intent.putExtra("ViewType", "assets");
                startActivity(intent);
            }
        });



        btn_open_storage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent browserPDF = new Intent(Intent.ACTION_GET_CONTENT);
                browserPDF.setType("application/pdf");
                browserPDF.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(browserPDF,"Seleccione el archivo"),PICK_PDF_CODE );
            }
        });



        btn_open_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                intent.putExtra("ViewType", "web");
                startActivity(intent);
            }
        });



        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descargaPDF();
//                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
//                intent.putExtra("ViewType", "assets");
//                startActivity(intent);
//                switch (view.getId()){
//                    R.id.
//                }
            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PDF_CODE && resultCode == RESULT_OK && data != null) {
            Uri selectedPDF = data.getData();
            Intent intent = new Intent(HomeActivity.this,MainActivity.class);
            intent.putExtra("ViewType", "storage");
            intent.putExtra("FileUri",selectedPDF.toString());
            startActivity(intent);
        }
    }





    @Override
    public void onBackPressed() {
        if  (count == 0){
            Toast.makeText(getApplicationContext(),"Presione de nuevo para salir",Toast.LENGTH_SHORT).show();
            count++;
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        new CountDownTimer(3000,1000){
            @Override
            public void onTick(long millisUnyilFinissed){

            }

            @Override
            public void onFinish() {
                count = 0;
            }
        }.start();
    }


    //descargar PDF
    public void descargaPDF(){
        String urlDownload = "http://celta.creandolazos.cl/catalogos/Manual-de-destinos-tur%C3%ADsticos-inteligentes.pdf";
//        Log.e("Paso por aqui", "descargaPDF");

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage("Descargando Catálogo...");
        new DescargarPDFAsyncTask(progressDialog).execute(urlDownload);
    }


    class DescargarPDFAsyncTask extends AsyncTask<String, Integer, String> {
        ProgressDialog progressDialog;

        DescargarPDFAsyncTask(ProgressDialog progressDialog){
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... urlPDF) {
            String urlDownload = urlPDF[0];

            HttpURLConnection conexion = null;
            InputStream input = null;
            OutputStream output = null;
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy", Locale.getDefault());
            Date date = new Date();
            String fecha = dateFormat.format(date);



            try {
                URL url = new URL(urlDownload);

                conexion = (HttpURLConnection) url.openConnection();
                conexion.connect();

                if (conexion.getResponseCode() != HttpURLConnection.HTTP_OK){
                    return "conexion no realizada correctamente";
                }

                input = conexion.getInputStream();
//                String rutaFichero = getFilesDir() + "/Catalogo" + fecha + ".pdf"; //  /data/data/com.luismarin.celtacys001/Catalogo.pdf
                String filename = "/Catalogo" + fecha + ".pdf";
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),filename);
                output = new FileOutputStream(file);

                int tamanoFichero = conexion.getContentLength();
                byte[] data = new byte[1024];
                int total = 0;
                int count;

                while((count = input.read(data)) != -1){
//                    sleep(1000);
                    output.write(data, 0, count);
                    total += count;
                    publishProgress((int) (total * 100 / tamanoFichero));
                }
            } catch (MalformedURLException e){
                e.printStackTrace();
                return "Error: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Error: " + e.getMessage();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
            } finally {
                try {
                    if (input != null) input.close();
                    if (output != null) output.close();
                    if (conexion != null) conexion.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return "Se descargo correctamente";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String mensaje) {
            super.onPostExecute(mensaje);
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
        }
    }

}

