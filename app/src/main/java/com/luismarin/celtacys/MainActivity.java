package com.luismarin.celtacys;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);

        //PDF View
        final PDFView pdfView = findViewById(R.id.pdfView);
        String viewType = getIntent().getStringExtra("ViewType");
        if (viewType.equals("assets")){
            pdfView.fromAsset("eBook_Whatsup_expresiones-en-ingles.pdf")
                    .pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    .spacing(0)
                    .pageFitPolicy(FitPolicy.WIDTH)
                    .load();
        }
        if (viewType.equals("storage")){
//            Intent intent = new Intent();
//            intent.setDataAndType(Uri.parse("http://celta.creandolazos.cl/catalogos/Manual-de-destinos-tur%C3%ADsticos-inteligentes.pdf"), "application/pdf");
//            startActivity(intent);

            Uri pdfFile = Uri.parse(getIntent().getStringExtra("FileUri"));
            pdfView.fromUri(pdfFile)
//            pdfView.fromAsset("eBook_Whatsup_expresiones-en-ingles.pdf")
                    .pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    .spacing(0)
                    .pageFitPolicy(FitPolicy.WIDTH)
                    .load();
        }
        if (viewType.equals("web")){
            progressBar.setVisibility(View.VISIBLE); //Show progressBar

            FileLoader.with(this)
                    .load("http://celta.creandolazos.cl/catalogos/Manual-de-destinos-tur%C3%ADsticos-inteligentes.pdf")
                    .fromDirectory("PDFFiles",FileLoader.DIR_EXTERNAL_PUBLIC)
                    .asFile(new FileRequestListener<File>() {
                        @Override
                        public void onLoad(FileLoadRequest fileLoadRequest, FileResponse<File> fileResponse) {
                            progressBar.setVisibility(View.GONE); //hide progressBar

                            File pdfFile = fileResponse.getBody();

                            pdfView.fromFile(pdfFile)
                                    .pages(0, 2, 1, 3, 3, 3) //all pages are displayed by default
                                    .enableSwipe(true) // allows to block changing pages using swipe
                                    .swipeHorizontal(false)
                                    .enableDoubletap(true)
                                    .defaultPage(0)
                                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                                    .password(null)
                                    .scrollHandle(null)
                                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                                    // spacing between pages in dp. To define spacing color, set view background
                                    .spacing(0)
                                    .pageFitPolicy(FitPolicy.WIDTH)
                                    .load();
                        }

                        @Override
                        public void onError(FileLoadRequest fileLoadRequest, Throwable throwable) {
                            Toast.makeText(MainActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE); //hide progressBar
                        }
                    });
        }



    }
}
